package com.example.ReactTwitter;
import org.springframework.context.annotation.Configuration;
import lombok.Data;
@Configuration
@Data
public class TweetJson {
    private String title;
    private String text;
}