package com.example.ReactTwitter.controller;
import com.example.ReactTwitter.entity.Messages;
import com.example.ReactTwitter.repository.MessageRepository;
import com.example.ReactTwitter.request.MessageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("ReactTwitter")
public class GetTweet {

    private final MessageRepository messageRepository;

    public GetTweet(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @CrossOrigin
    @PostMapping(value="/getTweet")
        public Messages getTweet(@RequestBody String json) throws JsonProcessingException {
        //System.out.println(json);
    // ----------ここから追加内容----------
    // ObjectMapperのインスタンス生成
        ObjectMapper mapper = new ObjectMapper();
    // JsonNodeに変換。(これによりkeyを指定することでvalueを取得することができる。)
        JsonNode node = mapper.readTree(json); // node={"title":"aaa","text":"ｂｂｂ"}
    // get()で指定した文字列をkeyにvalueを取得する。
        String title = node.get("title").textValue(); //title = タイトル
        String text = node.get("text").textValue(); //text = 本文

        Messages entity = new Messages();
        entity.setTitle(title);
        entity.setText(text);
        return messageRepository.save(entity);

    }
}
