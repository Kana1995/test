package com.example.ReactTwitter.controller;
import com.example.ReactTwitter.TweetJson;
import com.example.ReactTwitter.entity.Messages;
import com.example.ReactTwitter.repository.MessageRepository;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RestController;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("ReactTwitter")
public class SendTweet {

    private final MessageRepository messageRepository;

    public SendTweet(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @CrossOrigin
    @GetMapping(value="/sendTweet")
    public ResponseEntity<String> getAll() throws JsonProcessingException {

        //messagesの一覧を取得
        List<Messages> messages = messageRepository.findAll();

        TweetJson json = new TweetJson();
        json.setTitle("テスト");
        json.setText("テストの実施");

        // ObjectMapperのインスタンス生成
        ObjectMapper mapper = new ObjectMapper();
        // Stringに変換。 //jsonをListに変換する
        String jsonStr = mapper.writeValueAsString(messages); //jsonStr = {"title":"テスト","text":"テストの実施"}
        System.out.println(jsonStr);


        return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
    }
}