package com.example.ReactTwitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactTwitterApplication.class, args);
	}

}
